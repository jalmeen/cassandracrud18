public class Main {
    public static void main(String[] args) {
        Crud crud = new Crud();
//        Connection connection = new Connection();

        crud.connect("192.200.0.58", 32774);
        crud.getSession();

        crud.createKeyspace("sample", "SimpleStrategy", 3);

        crud.createTable();

        crud.insert();

        crud.read();

        crud.update();

//        crud.deleteKeyspace("sample");

        crud.close();
    }
}
